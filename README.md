### Start the services

./run start

(Or you can cd offline/online  npm install && npm start)

### Stop the services

./run stop

### :memo: Notes

The application is written in nodejs so, is ditributed in two different services:
- one service for online payments (and sync search with external payment api)
- one service for offline payments

I've kept two enpoints to check that the services are available http://localhost:3000 and http://localhost:3001 (this is not strictly necessary)

This separation is needed for multiple reasons, for example the fact that the online one it will probably need more resources (since is a sync process) and also because is a  nodejs app which is single threaded (you can the threads worker can be activated but it's still in test mode) and with a proper tool (like pm2) you can separate the resource (with the script is not guarantee that are actually separated).
If it was done with java (springboot) it could be in a single application with multiple services (and threads). (I've chosen node cause is more "hacky" but if you're interested in a java version I can provide one)

### :pushpin: Things to improve

Many things:

Tests (Right now "npm test" it does execute just an eslint check)

From DB point of view we could introduce a lock for the update of the accounts (I haven't seen any conflict though)

The two apps can be dockerized with the others part of the app (another two services) or you can use pm2 to distribute better the resources


