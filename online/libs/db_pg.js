const Sequelize = require('sequelize')

const config = require('../config');

// connection options
const options = {
    host: config.pg_host,
    dialect: 'postgres',
    logging: console.log,
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    define: {
        timestamps: false
    }
}


if (config.sequelize_logs !== true) {
    options.logging = config.sequelize_logs;
}

const db_pg = new Sequelize(config.pg_database, config.pg_username, config.pg_password, options);

db_pg
    .authenticate()
    .then(() => {
        console.log('Database connection has been established successfully on ' + config.pg_host);
    })
    .catch(function (err) {
        console.log('Unable to connect to the database:', err);
});

const db = {};

db.Sequelize = Sequelize;
db.db_pg = db_pg;

//db models
db.accounts = require('../models/accounts')(db_pg, Sequelize);
db.payments = require('../models/payments')(db_pg, Sequelize, db.accounts);

//Relations
db.payments.hasOne(db.accounts, { foreignKey: 'account_id', sourceKey: 'account_id' })
module.exports = db;