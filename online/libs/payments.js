const axios = require('axios');
const config = require('../config');
const logs = require('./logs');

const payments = {
  checkPayment: async (message) => {
    try {
      await axios.post(config.payment_provider + "/payment", message);
      return true;
    } catch({ response }) {
      logs.registerError(message.payment_id, "network", response.status);
      return false;
    }
  }
}

module.exports = payments;