var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  return res.status(200).json({ 'isAlive' : true });
});

module.exports = router;
