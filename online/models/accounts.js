module.exports = (dbpg, Sequelize) => {
  const Accounts = dbpg.define('accounts', {
      account_id: {
          type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      birthdate: {
        type: Sequelize.DATE
      },
      last_payment_date: {
        type: Sequelize.DATE
      },
      created_on: {
        type: Sequelize.DATE
      }
  });
  Accounts.removeAttribute('id');
  return Accounts;
}