const kafka = require('kafka-node');
const config = require('../config');
const payments = require('../libs/payments');
const db = require('../libs/db_pg');
const logs = require('../libs/logs');
const Payments = db.payments;
const Accounts = db.accounts;

try {
  const Consumer = kafka.Consumer;
  const client = new kafka.KafkaClient(config.kafka_server);
  let consumer = new Consumer(
    client,
    [{ topic: config.kafka_topic_online, partition: 0 }],
    { autoCommit: true }
  );
  consumer.on('message', async function(message) {
    const parsed = JSON.parse(message.value);
    delete parsed.delay;
    // check if payment is correct
    const res = await payments.checkPayment(parsed);
    if (res) {
      const payment = Payments.build(parsed)
      try {
          // save payment
          await payment.save();
          // update account
          await Accounts.update(
            { last_payment_date: Date.now() },
            { where: { account_id: payment.account_id }}
          )
      } catch(err) {
        //log db error
        logs.registerError(parsed.payment_id, "database", JSON.stringify(err.parent));
      }
    }
  })
  consumer.on('error', function(err) {
    console.log('error', err);
  });
}
catch(e) {
  console.log(e);
}