#!/bin/bash

if [ "$1" = "" ]; then
    echo "Usage: ./start start|stop"
fi

if [ "$1" = "start" ]; then
    cd online
    modules=$(ls -la | grep node_modules)
    if [ "$modules" = "" ]; then
        npm install
    fi

    export NODE_ENV=local
    npm start &

    cd ../offline
    modules=$(ls -la | grep node_modules)
    if [ "$modules" = "" ]; then
        npm install
    fi

    export NODE_ENV=local
    npm start &
fi

if [ "$1" = "stop" ]; then
    echo "..."
    killall node
    echo "All process are closed"
fi
