const axios = require('axios');
const config = require('../config');

const logs = {
  registerError: async (payment, error_type, error_desc) => {
    try {
      await axios.post(config.logs_server + "/log", {
        payment_id: payment,
        error_type: error_type,
        error_desc: error_desc
      });
    } catch(err) {
      console.log(err);
    }
  }
}

module.exports = logs;