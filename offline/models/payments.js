module.exports = (dbpg, Sequelize, Accounts) => {
  const Payment = dbpg.define('payments', {
      payment_id: {
          type: Sequelize.INTEGER
      },
      account_id: {
          type: Sequelize.INTEGER,
          references: {
            model: Accounts,
            key: "account_id"
        }
      },
      payment_type: {
        type: Sequelize.STRING
      },
      credit_card: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.DECIMAL
      },
      created_on: {
        type:  Sequelize.DATE,
      }
  });
  Payment.removeAttribute('id');
  return Payment;
}