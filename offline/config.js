var env = (process.env.NODE_ENV || 'local').toLowerCase();
var config = require('./config/' + env + '.json');

console.log('Environment: '+ env);

config.env = env;
module.exports = config;